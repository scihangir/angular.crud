import { Component, OnInit } from '@angular/core';

import {Book} from '../../models/book';

import {ActivatedRoute, Params, Router} from '@angular/router';

import {BookViewService} from '../../services/book-view.service';
import {BookInsertService} from '../../services/book-insert.service';

@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.css']
})
export class BookEditComponent implements OnInit {

  private bookId: number;
  private book: Book = new Book();

  constructor(
    private bookInsertService: BookInsertService,
    private bookViewService: BookViewService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) { }


  onSubmit() {
    this.bookInsertService.sendBook(this.book).subscribe(
      res => {
        console.log('Updated');
        console.log(res);
        this.router.navigate(['/bookList']);

      },
      error => {
        console.log(error);
      }
    );
  }

  ngOnInit() {
    // this.activatedRoute.params.forEach((params: Params) => {
    //   this.bookId = Number.parseInt(params['id']);
    // });
    // our obeservable fires then, we retrieve the updated
    // parameters and assign them to our bookId
    this.activatedRoute.params.subscribe(
      (params: Params) => {
        this.bookId = params['id'];
      }
    );

    this.bookViewService.getBook(this.bookId).subscribe(
      res => {
        this.book = res.json();
      },
      error => console.log(error)
    );
  }

}
