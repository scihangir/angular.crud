import { Component, OnInit } from '@angular/core';

import {Book} from '../../models/book';
import {BookInsertService} from '../../services/book-insert.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-book-insert',
  templateUrl: './book-insert.component.html',
  styleUrls: ['./book-insert.component.css']
})
export class BookInsertComponent implements OnInit {


  private newBook: Book = new Book();

  constructor(private bookInsertService: BookInsertService, private router: Router) { }

  onSubmit() {
    this.bookInsertService.sendBook(this.newBook).subscribe(
        res => {
            console.log('Added');
            console.log(res);
          this.router.navigate(['/bookList']);

        },
        error => {
          console.log(error);
        }
    );
  }

  ngOnInit() {
  }

}
