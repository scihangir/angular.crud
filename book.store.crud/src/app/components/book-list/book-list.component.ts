import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

import {Book} from '../../models/book';

import {BookListService} from '../../services/book-list.service';
import {BookRemoveService} from '../../services/book-remove.service';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {


  private bookList: Book[];


  constructor(
    private bookListService: BookListService,
    private bookRemoveService: BookRemoveService,
    private router: Router
  ) { }



  getBookList() {
    this.bookListService.getBookList().subscribe(
        res => {
            console.log(res.json());
            this.bookList = res.json();
        },
        error => {
            console.log(error);
        }
    );
  }

  onDelete(book: Book) {
    this.bookRemoveService.deleteBook(book).subscribe(
      res => {
        console.log(res);
        this.getBookList();
      },
      err => {
        console.log(err);
      }
    );
  }

  ngOnInit() {
    this.getBookList();
  }

}
