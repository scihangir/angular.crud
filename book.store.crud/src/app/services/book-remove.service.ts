import { Injectable } from '@angular/core';
import {Book} from '../models/book';
import {Http, Headers} from '@angular/http';

@Injectable()
export class BookRemoveService {

  constructor(private http: Http) { }

  deleteBook(book: Book) {
    let url = 'http://localhost:8080/book/remove';
    const headers = new Headers({ 'Content-Type': 'application/json' });

    return this.http.post(url, book.id, {headers: headers });
  }
}
