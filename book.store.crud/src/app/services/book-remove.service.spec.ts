import { TestBed, inject } from '@angular/core/testing';

import { BookRemoveService } from './book-remove.service';

describe('BookRemoveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BookRemoveService]
    });
  });

  it('should be created', inject([BookRemoveService], (service: BookRemoveService) => {
    expect(service).toBeTruthy();
  }));
});
