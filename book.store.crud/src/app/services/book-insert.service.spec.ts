import { TestBed, inject } from '@angular/core/testing';

import { BookInsertService } from './book-insert.service';

describe('BookInsertService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BookInsertService]
    });
  });

  it('should be created', inject([BookInsertService], (service: BookInsertService) => {
    expect(service).toBeTruthy();
  }));
});
