import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';

@Injectable()
export class BookListService {

  constructor(private http: Http) { }

  getBookList() {
    let url = 'http://localhost:8080/book/bookList';
    const headers = new Headers({ 'Content-Type': 'application/json' });

    return this.http.get(url, {headers: headers });
  }

}
