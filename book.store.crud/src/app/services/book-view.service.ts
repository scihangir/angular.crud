import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http' ;

@Injectable()
export class BookViewService {

  constructor(private http: Http) { }

  getBook(id: number) {
    let url = 'http://localhost:8080/book/' + id;
    const headers = new Headers({ 'Content-Type': 'application/json' });

    return this.http.get(url, {headers: headers});
  }

}
