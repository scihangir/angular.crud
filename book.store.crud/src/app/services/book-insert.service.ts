import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';

import {Book} from '../models/book';

@Injectable()
export class BookInsertService {

  constructor(private http: Http) { }

  sendBook(book: Book) {
    let url = 'http://localhost:8080/book/add';
    const headers = new Headers({ 'Content-Type': 'application/json' });

    return this.http.post(url, JSON.stringify(book), {headers: headers} );
  }

}
