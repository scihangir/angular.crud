import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing } from './app.routing';

import { AppComponent } from './app.component';
import { BookListComponent } from './components/book-list/book-list.component';
import { BookInsertComponent } from './components/book-insert/book-insert.component';
import { BookEditComponent } from './components/book-edit/book-edit.component';

import {BookListService} from './services/book-list.service';
import {BookInsertService} from './services/book-insert.service';
import {BookViewService} from './services/book-view.service'  ;
import {BookRemoveService} from './services/book-remove.service';

@NgModule({
  declarations: [
    AppComponent,
    BookListComponent,
    BookInsertComponent,
    BookEditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [BookListService, BookInsertService, BookViewService, BookRemoveService],
  bootstrap: [AppComponent]
})
export class AppModule { }
