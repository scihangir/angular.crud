/**
 * Created by cihangir on 8/3/17.
 */

import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {BookListComponent} from './components/book-list/book-list.component';
import {BookInsertComponent} from './components/book-insert/book-insert.component';
import {BookEditComponent} from './components/book-edit/book-edit.component';


const appRoutes: Routes = [
  {
    path : '',
    redirectTo: '/',
    pathMatch: 'full'
  },
  {
    path: 'bookList',
    component: BookListComponent
  },
  {
    path: 'addBook',
    component: BookInsertComponent
  },
  {
    path: 'editBook/:id',
    component: BookEditComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
