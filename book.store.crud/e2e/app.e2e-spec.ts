import { Book.Store.CrudPage } from './app.po';

describe('book.store.crud App', () => {
  let page: Book.Store.CrudPage;

  beforeEach(() => {
    page = new Book.Store.CrudPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
